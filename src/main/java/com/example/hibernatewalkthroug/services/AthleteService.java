package com.example.hibernatewalkthroug.services;


import com.example.hibernatewalkthroug.model.athleteSystem.Athlete;

public interface AthleteService extends CrudService<Athlete, Integer> {


        void setCoach(int athlete_id, int coach_id);

        void setEvent(int athlete_id, int event_id);
}
