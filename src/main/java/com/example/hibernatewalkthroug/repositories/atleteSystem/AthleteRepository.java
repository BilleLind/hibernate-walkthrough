package com.example.hibernatewalkthroug.repositories.atleteSystem;


import com.example.hibernatewalkthroug.model.athleteSystem.Athlete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface AthleteRepository extends JpaRepository<Athlete, Integer> {

    @Query("SELECT a FROM Athlete a WHERE a.name LIKE %?1%")
    Set<Athlete> findAllByFirstName(String name);

    @Modifying
    @Query("update Athlete at set at.coach.id= ?2 where at.id =?1")
    void updateCoachById(int athlete_id, int coach_id);

    @Modifying
    @Query(value = "update athlete_events set events_id= ?1, athletes_id =?2", nativeQuery = true)
    void updateEvent(int event_id, int athlete_id);
}
