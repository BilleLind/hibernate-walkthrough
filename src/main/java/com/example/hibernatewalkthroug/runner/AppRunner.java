package com.example.hibernatewalkthroug.runner;

import com.example.hibernatewalkthroug.model.athleteSystem.Athlete;
import com.example.hibernatewalkthroug.repositories.atleteSystem.AthleteRepository;
import com.example.hibernatewalkthroug.services.AthleteService;
import com.example.hibernatewalkthroug.services.CoachService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Client class for the Services.
 * This class exists to demonstrate the use of services.
 * It implements ApplicationRunner to be able to use dependency injection.
 */
@Component
public class AppRunner implements ApplicationRunner {

    private final AthleteService athleteService;
    private final CoachService coachService;

    private final AthleteRepository athleteRepository;

    public AppRunner(AthleteService athleteService, CoachService coachService, AthleteRepository athleteRepository) {
        this.athleteService = athleteService;
        this.coachService = coachService;
        this.athleteRepository = athleteRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //coachRepository.deleteById(1);
        //Collection<Athlete> athletes = athleteService.findAll();
        //athletes.forEach(s-> System.out.println(s.getName()));
        athleteService.setCoach(1, 3);
        //System.out.println(athleteRepository.findAllByFirstName(""));

        System.out.println(athleteRepository.findAllByFirstName("2"));
    }
}
