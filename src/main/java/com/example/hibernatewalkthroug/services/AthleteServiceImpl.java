package com.example.hibernatewalkthroug.services;

import com.example.hibernatewalkthroug.model.athleteSystem.Athlete;
import com.example.hibernatewalkthroug.repositories.atleteSystem.AthleteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class AthleteServiceImpl implements AthleteService{
    private final AthleteRepository athleteRepository;
    private final Logger logger = LoggerFactory.getLogger(AthleteServiceImpl.class);

    public AthleteServiceImpl(AthleteRepository athleteRepository) {
        this.athleteRepository = athleteRepository;
    }


    @Override
    @Transactional
    public void setCoach(int athlete_id, int coach_id) {
        this.athleteRepository.updateCoachById(athlete_id, coach_id);
    }

    @Override
    @Transactional
    public void setEvent(int athlete_id, int event_id) {
        athleteRepository.updateEvent(event_id, athlete_id);
    }

    @Override
    public Athlete findById(Integer id) {
        return athleteRepository.findById(id).get();
    }

    @Override
    public Collection<Athlete> findAll() {
        return athleteRepository.findAll();
    }

    @Override
    public Athlete add(Athlete athlete) {
        return athleteRepository.save(athlete);
    }

    @Override
    public Athlete update(Athlete athlete) {
        return athleteRepository.save(athlete);
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (athleteRepository.existsById(id)) {

            athleteRepository.deleteById(id);
        }
    }



}
