-- Coach
INSERT INTO coach ("name") VALUES ('Coach 1');
INSERT INTO coach ("name") VALUES ('Coach 2');
INSERT INTO coach ("name") VALUES ('Coach 3');

-- Athlete
INSERT INTO athlete (name, coach_id) VALUES ('Athelete 1', 1);
INSERT INTO athlete (name, coach_id) VALUES ('Athelete 2', 2);
INSERT INTO athlete (name, coach_id) VALUES ('Athelete 3', 3);

-- Certificate
INSERT INTO certificate (title) values ('Certificate 1');
INSERT INTO certificate (title) values ('Certificate 2');
INSERT INTO certificate (title) values ('Certificate 3');

-- Event
INSERT INTO event (name) values ('Event 1');
INSERT INTO event (name) values ('Event 2');
INSERT INTO event (name) values ('Event 3');

-- coach_certificates
insert into coach_certificates (coaches_id, certificates_id) VALUES (1,3);
insert into coach_certificates (coaches_id, certificates_id) VALUES (2,1);
insert into coach_certificates (coaches_id, certificates_id) VALUES (2,3);
insert into coach_certificates (coaches_id, certificates_id) VALUES (3,3);

-- athlete_events
insert into athlete_events (athletes_id, events_id) VALUES (1,1);
insert into athlete_events (athletes_id, events_id) VALUES (2,1);
insert into athlete_events (athletes_id, events_id) VALUES (2,2);
insert into athlete_events (athletes_id, events_id) VALUES (3,2);
insert into athlete_events (athletes_id, events_id) VALUES (3,3);


