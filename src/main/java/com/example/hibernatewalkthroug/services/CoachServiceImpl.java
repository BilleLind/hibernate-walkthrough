package com.example.hibernatewalkthroug.services;

import com.example.hibernatewalkthroug.model.athleteSystem.Certificate;
import com.example.hibernatewalkthroug.model.athleteSystem.Coach;
import com.example.hibernatewalkthroug.repositories.atleteSystem.CoachRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
public class CoachServiceImpl implements CoachService{
    private final Logger logger = LoggerFactory.getLogger(CoachServiceImpl.class);
    private final CoachRepository coachRepository;

    public CoachServiceImpl(CoachRepository coachRepository){
        this.coachRepository = coachRepository;
    }

    @Override
    public Coach findById(Integer integer) {
        return null;
    }

    @Override
    public Collection<Coach> findAll() {
        return null;
    }

    @Override
    public Coach add(Coach entity) {
        return null;
    }

    @Override
    public Coach update(Coach entity) {
        return null;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        if (coachRepository.existsById(id)) {
            Coach coach = coachRepository.findById(id).get();
            coach.getAthletes().forEach(s ->s.setCoach(null));
            // removal of nonOwned certificates (removes certificates of the coach
           // for (Certificate certificate : coach.getCertificates()){
            //    certificate.getCoaches();}
            coach.getCertificates().forEach(cert ->cert.removeCoach(coach));
            coachRepository.delete(coach);
        } else {
            logger.warn("No coach exists with ID: " + id);
        }
    }


}
