package com.example.hibernatewalkthroug;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HibernateWalkthrougApplication {

    public static void main(String[] args) {
        SpringApplication.run(HibernateWalkthrougApplication.class, args);
    }

}
