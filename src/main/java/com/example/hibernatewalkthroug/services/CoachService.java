package com.example.hibernatewalkthroug.services;

import com.example.hibernatewalkthroug.model.athleteSystem.Coach;
import org.springframework.stereotype.Service;

@Service
public interface CoachService extends CrudService<Coach, Integer>{


}
