package com.example.hibernatewalkthroug.model.athleteSystem;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 60, nullable = false)
    private String name;

    @ManyToMany(mappedBy = "events")
    private Set<Athlete> athletes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Athlete> getAthletes() {
        return athletes;
    }

    public void setAthletes(Set<Athlete> athletes) {
        this.athletes = athletes;
    }

    public void addAthlete(Athlete athlete) {
        this.athletes.add(athlete);
        athlete.getEvents().add(this);
    }
    public void removeAthlete(Athlete athlete) {
        this.athletes.remove(athlete);
        athlete.getEvents().remove(this);
    }
}
