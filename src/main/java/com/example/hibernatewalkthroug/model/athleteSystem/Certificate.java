package com.example.hibernatewalkthroug.model.athleteSystem;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Certificate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 80, nullable = false)
    private String title;

    @ManyToMany(mappedBy = "certificates")
    private Set<Coach> coaches;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Coach> getCoaches() {
        return coaches;
    }

    public void setCoaches(Set<Coach> coaches) {
        this.coaches = coaches;
    }


    //Utility methods to deal with many-to-many delete
    //from https://thorben-janssen.com/hibernate-tips-the-best-way-to-remove-entities-from-a-many-to-many-association/
    public void addCoach(Coach coach) {
        this.coaches.add(coach);
        coach.getCertificates().add(this);
    }
    public void removeCoach(Coach coach) {
        this.coaches.remove(coach);
        coach.getCertificates().remove(this);
    }
}
